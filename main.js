// 随机颜色
const randomColor = () => {
    return `rgb(${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)})`
}

// 随机生成2个数之间的一个整数
const randomNum = (min, max) => {
    return Math.floor((Math.random() * (max - min)) + min)
}

// 获取画板
const can = document.querySelector('canvas')
// 获取画笔
const ctx = can.getContext('2d')
// 存球的数组
let balls = []
// 球的个数
const ballNum = randomNum(100, 200)


// 球类
class Ball {
    constructor(x, y, r, color, dx, dy) {
        this.x = x
        this.y = y
        this.r = r
        this.color = color
        this.dx = dx
        this.dy = dy
    }

    // 更新球的状态
    update() {
        this.dx = (this.x + this.r) > can.width || (this.x - this.r) < 0 ? -this.dx : this.dx
        this.dy = (this.y + this.r) > can.height ? -this.dy * 0.98 : this.dy += 1
        this.y += this.dy * 0.98
        // 当前的y坐标和球半径的值相加小于可视区域的高度时让x坐标移动
        if (this.y + this.r < can.height) {
            this.x += this.dx
        }
        this.draw()
    }

    // 绘制球
    draw() {
        ctx.beginPath()
        ctx.fillStyle = this.color
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)
        ctx.fill()
        ctx.stroke()
        ctx.closePath()
    }
}

// 初始化
const init = () => {
    // 设置画布宽高
    can.width = window.innerWidth
    can.height = window.innerHeight
    // 先清空数组
    balls.length = 0
    // 添加球到数组里面
    for (let i = 0; i < ballNum; i++) {
        const r = randomNum(20, 40)
        const x = randomNum(i, can.width - (2 * r))
        const y = randomNum(i, (can.height / randomNum(0, 5)) - (2 * r))
        const color = randomColor()
        const dx = randomNum(5, 10)
        const dy = randomNum(5, 10)
        balls.push(new Ball(x, y, r, color, dx, dy))
    }
}
init()

// 改变窗口大小重新初始化
window.onresize = () => {
    init()
}

// 定时动画
const animation = () => {
    requestAnimationFrame(animation)
    // 清空画布
    ctx.clearRect(0, 0, can.width, can.height)
    // 遍历存球的数组
    balls.forEach(ball => {
        // 更新每个球的状态
        ball.update()
    })
}
animation()

